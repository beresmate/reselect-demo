import { applyMiddleware, createStore, combineReducers } from 'redux';
import { createLogger } from 'redux-logger';
import { createSelector } from 'reselect';

/**
 *  ACTION
 */
const NOBODY_CARES_CHANGED = 'NOBODY_CARES_CHANGED';

function nobodyCaresAction() {
    return {
        type: NOBODY_CARES_CHANGED
    };
}


/**
 * REDUCERS
 *
 * 2 darab lesz: events es nobodyCares
 */
function eventsReducer(state = [1, 2, 3], action) {
    switch (action.type) {
        case 'NO_ONE_DISPATCHES_ME_I_AM_A_SAD_EVENT_ID':
            return action.payload;
        default:
            return state;
    }
}

function nobodyCaresReducer(state = 0, action) {
    switch (action.type) {
        case NOBODY_CARES_CHANGED:
            return ++state;
        default:
            return state;
    }
}


/**
 * SELECTORS
 *
 * az elso lassu CPU-t szimulal, mint pld a moment parse/format
 */
const expensiveLengthCalculation = list => {
    let i, output = '';

    for (i = 1; i <= 1e7; i++) {
        output += i;
    }

    return list.length;
};
const eventsSelector = state => state.events;

// ez csak siman meg van hivva az eventsSelector kimenetevel
const eventcountSelector = state => expensiveLengthCalculation(eventsSelector(state));

// ez meg a reselect.createSelector-on keresztul kapja meg az eventsSelector kimenetet
// ezert ha annak a kimenete nem valtozik, akkor o sem fut le ujra
const improvedEventcountSelector = createSelector(eventsSelector, expensiveLengthCalculation);

/**
 * STORE
 */
const store = createStore(
    combineReducers({
        events: eventsReducer,
        nobodyCares: nobodyCaresReducer
    }),
    applyMiddleware(
        createLogger({ collapsed: true })
    )
);


/**
 * MAPSTATE HELYETT SUBSCRIBE
 */
store.subscribe(() => {
    console.time('Counting events');

    // 1. reselect nelkul
    // console.log('Event count without reselect:', eventcountSelector(store.getState()));

    // 2. reselect-tel
    console.log('Event count with reselect:', improvedEventcountSelector(store.getState()));

    console.timeEnd('Counting events');
});


/**
 * ACTION-OK INDITASA
 *
 * itt az a lenyeg, hogy a nobodyCaresAction-re nem valtozik a state.events
 * ezert nem kellene hogy ujraszamolodjon minden dispatch-kor az a selector,
 * ami a state.events-bol dolgozik
 */
const interval = window.setInterval(() => {
    store.dispatch(nobodyCaresAction());
}, 500);

// itt a vege, RIP
window.setTimeout(() => {
    window.clearInterval(interval);
    console.log('Nobody cares action stopped. RIP')
}, 4000);
